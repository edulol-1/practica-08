/**
* InvalidRangeException.
* @author Eduardo Montaño Gómez.
* Numero de cuenta: 421005183
* @since Laboratorio ICC. 2021-1.
* @version 1.0 Enerio 2021.
*/
public class EmptyPhraseException extends Exception{
	
	/**
	* Metodo constructor vacio.
	*/
	public EmptyPhraseException() {
		super("Cadena vacia");
	}

	/**
	* Crea una nueva EmptyPhraseException
	* @param message el mensaje.
	*/
	public EmptyPhraseException(String message) {
		super(message);
	}
}
