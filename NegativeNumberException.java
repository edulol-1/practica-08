/**
* InvalidRangeException.
* @author Eduardo Montaño Gómez.
* Numero de cuenta: 421005183
* @since Laboratorio ICC. 2021-1.
* @version 1.0 Enerio 2021.
*/
public class NegativeNumberException extends Exception{
	
	/**
	* Metodo constructor vacio.
	*/
	public NegativeNumberException() {
		super("No se permiten negativos");
	}

	/**
	* Crea una nueva NegativeNumberException.
	* @param message el mensaje.
	*/
	public NegativeNumberException(String message) {
		super(message);
	}
}
