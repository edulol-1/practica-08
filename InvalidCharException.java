/**
* InvalidRangeException.
* @author Eduardo Montaño Gómez.
* Numero de cuenta: 421005183
* @since Laboratorio ICC. 2021-1.
* @version 1.0 Enerio 2021.
*/
public class InvalidCharException extends Exception{
	
	/**
	* Metodo constructor vacio.
	*/
	public InvalidCharException() {
		super("Caracter no valido");
	}

	/**
	* Crea una nueva InvalidCharException
	* @param message el mensaje.
	*/
	public InvalidCharException(String message) {
		super(message);
	}
}
