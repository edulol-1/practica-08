/**
* @author Eduardo Montaño Gomez.
* Numero de cuenta: 421005183
* @since Laboratorio ICC. 2021-1.
* @version 1.0 Enerio 2021.
*/
import java.util.Scanner;
import java.util.InputMismatchException;
public class Recursivo implements RecursivoInterfaz{
	//Scanner sc = new Scanner();
	
	@Override
	public boolean isPalindrome(int x) throws NegativeNumberException {
		if (x < 0)//Arrojamos una excepcion en caso de que se obtenga un numero negativo.
			throw new NegativeNumberException("Numero negativo");
		//Si el numero es cero, regresamos true.
		if (x==0)
			return true;

		int respuesta = 0;
		//System.out.println(reversa(x));
		if (reversa(x) == x)
			return true;
		else
			return false;
	}

	/*Metodo auxiliar que regresa al reversa de un numero*/
	private int reversa(int numero) {
		//Verificamos si el numero esta entre 10 y 0.
		if (numero < 10 && numero > 0)
			return numero;
		String num = numero + "";//Creamos una cadena auxiliar.
		int multiplo10 = 1;//
		int ultimoDigito = numero%10; //Obtenemos el ultimo digito del valor.
		numero = numero/10;//Dividimos el numero entre diez para quitar el ultimo digito.
		//Multiplicamos por su respectiva potencia de 10.
		ultimoDigito = (ultimoDigito * potencia10(num));
		return ultimoDigito + reversa(numero);
	}

	/*Regresa la potencia n-esima de 10*/
	private int potencia10(String n) {
		//Caso base
		if (n.length()==1)
			return 1;
		//Regla recursiva
		return 10*potencia10(n.substring(1)); 
	}
	
	@Override
	public boolean checkRecord(String s) throws InvalidCharException {
		//Verificamos si es que es un caracter no valido.
		if (!verificaCaracter(s))
			throw new InvalidCharException();
		//Si el total de apariciones de 'A' es al menos una: false.
		if (ocurrencias(s, 'A') >= 1)
			return false;
		//SI el total de apariciones de 'R' es al menos dos: false.
		if (ocurrencias(s,'R') >= 2)
			return false;

		return true;
	}

	/*Cuenta la cantidad de apariciones de un caracter.*/
	private int ocurrencias(String s, char c) {
		//Caso base.
		if (s.length()==0)
			return 0;

		//Caso recursivo.
		if (s.charAt(0) != c)//Si el caracter no es igual.
			return ocurrencias(s.substring(1), c);
		else//Si el caracter es igual.
			return 1 + ocurrencias(s.substring(1), c);
	}

	/*Verifica que los caracteres sean validos.*/
	private boolean verificaCaracter(String s) {
		//Verificamos si es de longitud 0.
		if (s.length()==0)
			return false;
		//Verificamos si es valido.
		if (s.charAt(0) != 'A' && s.charAt(0) != 'R' && s.charAt(0) != 'P')
			return false;
		//Caso recursivo.
		return true && verificaCaracter(s.substring(1, s.length()));
	}
	
	@Override
	public int addDigits(int num) throws InvalidRangeException {
		//Vemos si es que esta en el rango.
		if (num < 0 || num > 99) 
			throw new InvalidRangeException("Fuera de rango");
		//Vemos si está entre 0 y 10.
		if (num < 10 && num >= 0)
			return num;
		int segundo = num%10;//Obtenemos el segunto dígito.
		int primero = num/10;//Obtenemos el primer dígito.
		//Caso recursivo.
		return addDigits(primero + segundo);
	}
	
	@Override
	public int lengthOfLastWord(String phrase) throws EmptyPhraseException {

		if (phrase.equals(""))//Arrojamos excepcion.
			throw new EmptyPhraseException("Cadena vacia");

		phrase = espaciosFinal(phrase);//Quitamos los espacios al final de una frase.
		//Obtenemos la ultima palabradespues del ultimo espacio en blanco.
		phrase = phrase.substring(phrase.length()-indiceUltimoEspacio(phrase));
		return phrase.length();
	}

	/*Quita los espacios al final de una palabra*/
	private String espaciosFinal(String cadena) {
		//Verificamos si el ultimo caracter es un espacio.
		if (cadena.charAt(cadena.length()-1) == ' ') {
			//Devolvemos la cadena sin el ultimo caracter.
			return espaciosFinal(cadena.substring(0, cadena.length()-1));
		} else {//SI no, regresamos la cadena normal.
			return cadena;
		}
	}

	/*Regresa el indice donde se encuentra el primer espacio de derecha a izquierda*/
	private int indiceUltimoEspacio(String s) {
		//Caso base.
		if (s.equals(""))
			return 0;

		//Caso recursivo.
		if (s.charAt(s.length()-1) != ' ')
			return 1 + indiceUltimoEspacio(s.substring(0, s.length()-1));
		else
			return 0;
	}

	/**
	* Menu recursivo.
	*/
	private void menu() {
		Scanner sc = new Scanner(System.in);
		System.out.print("\n\n\t\tMENU");
		System.out.print("\n\t[1]isPalindrome" + "\n\t[2]checkRecord" + "\n\t[3]addDigits" +
			"\n\t[4]lengthOfLastWord" + "\n\t[5]Salir");
		System.out.print("\n\tIngresar: ");
		int op=0;//Creamos la opcion.
		try {
			op = sc.nextInt();
			sc.nextLine();
		}catch(InputMismatchException imme) {
			System.out.println("\tSolo ingresar numeros enteros!");
		}catch (Exception e) {
			System.out.println("\tExcepcion del tipo " + e.getMessage());
		}

		switch (op) {
			case 1:
				System.out.print("\n\t\tisPalindrome");
				System.out.print("\n\tIngresa el numero: ");
				int numPalindrome = 0;
				try {//Validamos que sea un numero entero.
					numPalindrome = sc.nextInt();
					sc.nextLine();
					System.out.print("\n\tTu entrada --> " + numPalindrome);
					System.out.println("\n\tSalida --> " + isPalindrome(numPalindrome));
				}catch(InputMismatchException imme) {
					System.out.println("\tSolo ingresar numeros enteros!");
				}catch(NegativeNumberException nne) {
					System.out.println("\n\t" + nne.getMessage());
				}catch (Exception e) {
					System.out.println("\n\tExcepcion del tipo " + e.getMessage());
				}

				break;
			case 2:
				System.out.println("\n\t\tcheckRecord");
				System.out.print("\n\tIngresa la cadena: ");
				try {
					String cadena = sc.nextLine();
					boolean recompensa = checkRecord(cadena);
					if (recompensa) {
						System.out.print("\n\tEl alumno es recompensado!");

					}else {
						System.out.print("\n\tEl alumno no es recompensado :(");

					}
				}catch (InvalidCharException ice) {
					System.out.print("\n\t"+ ice.getMessage());
				} catch (Exception e) {
					System.out.print("\n\t"+ e.getMessage());					
				}
				break;
			case 3:
				System.out.print("\n\t\taddDigits");
				System.out.print("\n\tIngresa el valor numerico: ");
				int valor = 0;
				try {
					valor = sc.nextInt();
					sc.nextLine();
					System.out.println("\n\tSalida --> " + addDigits(valor));
				}catch (InputMismatchException imme) {
					System.out.print("\n\tSolo valores enteros!");
				}catch(InvalidRangeException ire) {
					System.out.print("\n\t" + ire.getMessage());
				}catch (Exception e) {
					System.out.print("\n\tExcepcion del tipo " + e.getMessage());
				}

				break;
			case 4:
				System.out.print("\n\t\tlengthOfLastWord");
				System.out.print("\n\tIngresa la cadena: ");
				try {
					String phrase = sc.nextLine();
					System.out.print("\n\tSalida --> " + lengthOfLastWord(phrase));
				} catch (EmptyPhraseException epe) {
					System.out.print("\n\t" + epe.getMessage());
				} catch (Exception e) {
					System.out.print("\n\t" + e.getMessage());
				}
				break;
			case 5:
				System.out.println("\tNos vemos!");
				return;
			default:
				System.out.print("\n\tNo hay esa opcion!");

		}
		menu();
		return;

	}

	public static void main(String[] args) {
		Recursivo rc = new Recursivo();
		rc.menu();


	}	
}
