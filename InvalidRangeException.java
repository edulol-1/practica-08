/**
* InvalidRangeException.
* @author Eduardo Montaño Gómez.
* Numero de cuenta: 421005183
* @since Laboratorio ICC. 2021-1.
* @version 1.0 Enerio 2021.
*/
public class InvalidRangeException extends Exception{
	
	/**
	* Metodo constructor vacio.
	*/
	public InvalidRangeException() {
		super("Fuera de rango");
	}

	/**
	* Crea una nueva InvalidRangeException.
	* @param message el mensaje.
	*/
	public InvalidRangeException(String message) {
		super(message);
	}
}
